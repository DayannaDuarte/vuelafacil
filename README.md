# VuelaFacil
La empresa de aviación vuelafacil.com nos contrató para desarrollar una aplicación, para la gestión de los vuelos. Específicamente que le permita buscar una ruta para ir de una ciudad a otra, usando únicamente los vuelos de los que dispone la empresa. El propósito es utilizar este programa desde todas las agencias de viaje del país.
De acuerdo con cifras de la Asociación Colombiana de Agencias de Viajes y Turismo (ANATO), actualmente existen en Colombia 11.185 agencias de viajes 

## Authors and acknowledgment
##### Dayanna Katherine Duarte Triana
##### Maria Camila Lopez Triana
##### Ana Maria Daza
##### Ferney Cagueñas
##### Andres Mendez

## License
Código abierto.

## Project status
En desarrollo.
