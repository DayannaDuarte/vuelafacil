package com.example.VuelaFacil;

import com.example.VuelaFacil.modelos.Ticket;
import com.example.VuelaFacil.modelos.Usuario;
import com.example.VuelaFacil.modelos.Vuelo;
import com.google.gson.Gson;
import java.sql.SQLException;
import java.util.List;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

// PETICIONES HTTP AL SERVIDOR:
// Get: Consultar
// Post: Actualizar o Insertar
// Put: Insertar
// Delete: Eliminar

@SpringBootApplication // Para indicar que en este archivo se encuentra la clase main
@RestController // Indica que la clase sera una API REST, la cual se encarga de hacer peticiones a un servidor
public class VuelaFacilApplication {

    // Instanciacion:
    @Autowired(required = true) // @Autowired evita declarar el objeto con Usuario u = new Usuario()
    Usuario u;
    @Autowired(required = true)
    Vuelo v;
    @Autowired(required = true)
    Ticket t;

    public static void main(String[] args) {
        SpringApplication.run(VuelaFacilApplication.class, args);
    }

    @GetMapping("/usuario") // Es una anotacion de SpringBoot que nos permite simplificar el manejo de los diferentes metodos de Spring MVC (Modelo Vista Controlador)
    public String consultarUsuarioPorNombre(@RequestParam(value = "nombre_usuario", defaultValue = "") String nombre_usuario) throws ClassNotFoundException, SQLException {
        u.setNombre_usuario(nombre_usuario);
        if (u.consultarUsuario()) {
            String res = new Gson().toJson(u);
            u.setId_usuario(0); // Si hay mas registros traigase el primero que encuentre
            u.setNombre_usuario("");
            u.setApellido_usuario("");
            u.setContrasenia_usuario("");
            u.setCorreo_usuario("");
            return res; // Retorna un json para la peticion al servidor
        } else {
            return new Gson().toJson(u);
        }
    }

    @GetMapping("/vuelodestino")
    public String consultarVueloPorDestino(@RequestParam(value = "destino", defaultValue = "1") String destino) throws ClassNotFoundException, SQLException {
        v.setOrigen(destino);
        if (v.consultarVueloExiste()) { // TRUE
            return new Gson().toJson(v); //  JSON
        } else { // 
            return new Gson().toJson(v);
        }
    }

    //---------------------------------------------------------------------------------------------------------------------
    //------------------------------CODIGO EXTRAIDO DE REPOSITORIO DE LA PROFESORA-----------------------------------------
    //---------------------------------------------------------------------------------------------------------------------
    
    @GetMapping("/tickets")
    public String consultarTickets(@RequestParam(value = "id_usuario", defaultValue = "1") int id_usuario) throws ClassNotFoundException, SQLException {
        List<Ticket> tickets = t.consultarTodo(id_usuario);
        if (!tickets.isEmpty()) {
            return new Gson().toJson(tickets);
        } else {
            return new Gson().toJson(tickets); // Retornará el JSON vacio.
        }
    }

    // GET - POST - PUT - DELETE
    // POST: Enviar un dato al servidor, también podemos efectuar cambios enviando la petición post al servidor(Actualización)
    // PUT: Actualizar (Reemplazar)
    @PostMapping(path = "/usuario", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
            produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definicion
    // RequestBody: Recibe los datos que enviaremos en el cuerpo de la petición utilizando HTTP POST
    // Le enviamos datos al servidor y el los resgitra en la BD
    // RequestBody sirve para hacer actualizaciones y guardar datos nuevos en la BD 
    public String actualizarUsuario(@RequestBody String usuario) throws ClassNotFoundException, SQLException {
        Usuario f = new Gson().fromJson(usuario, Usuario.class); // recibimos el json y lo devolvemos un objeto estudiante
        u.setId_usuario(f.getId_usuario()); // obtengo el id de f y se lo ingreso a la clase estudiante
        u.setNombre_usuario(f.getNombre_usuario()); // Toma el dato que encuentra en el json y luego se lo pasa a la clase Estudiante
        u.setApellido_usuario(f.getApellido_usuario());
        u.setContrasenia_usuario(f.getContrasenia_usuario());
        u.setCorreo_usuario(f.getCorreo_usuario());
        u.actualizarUsuario(); // ejecuta el método actualizar con los datos que se le enviaron del json y actualiza la BD
        return new Gson().toJson(u); // cliente le envia json al servidor. fpr de comunicarse

    }

    // Guardar una cuenta
    @DeleteMapping("/eliminarticket/{id_usuario}")
    public String eliminarTicket(@PathVariable("id_usuario") int id_usuario) throws SQLException, ClassNotFoundException {
        t.setId_usuario(id_usuario);
        t.borrarTicket(); // Este metodo borra el ticket con el id del usuario
        return "Los datos del id indicado han sido eliminados";
    }

    @PostMapping(path = "/guardarusuario", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String insertarUsuario(@RequestBody String usuario) throws ClassNotFoundException, SQLException {
        Usuario user = new Gson().fromJson(usuario, Usuario.class);
        u.setNombre_usuario(user.getNombre_usuario());
        u.setApellido_usuario(user.getApellido_usuario());
        u.setContrasenia_usuario(user.getContrasenia_usuario());
        u.guardarUsuario();
        return new Gson().toJson(u);
    }

}
