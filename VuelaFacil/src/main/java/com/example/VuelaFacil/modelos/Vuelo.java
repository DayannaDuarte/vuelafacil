package com.example.VuelaFacil.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grupo O5-01
 */
@Component("vuelo")
public class Vuelo {
    
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    
    // Atributos:
    private int id_vuelo;    
    private String matricula_aeronave;    
    private String aerolinea;    
    private String origen;    
    private String destino;    
    private double hora_vuelo;    
    private double fecha_vuelo;
    
    // Metodo constructor:

    public Vuelo(int id_vuelo, String matricula_aeronave, String aerolinea, String origen, String destino, double hora_vuelo, double fecha_vuelo) {
        this.id_vuelo = id_vuelo;
        this.matricula_aeronave = matricula_aeronave;
        this.aerolinea = aerolinea;
        this.origen = origen;
        this.destino = destino;
        this.hora_vuelo = hora_vuelo;
        this.fecha_vuelo = fecha_vuelo;
    }

    public Vuelo() {
    }
    
    // Metodos;

    public int getId_vuelo() {
        return id_vuelo;
    }

    public void setId_vuelo(int id_vuelo) {
        this.id_vuelo = id_vuelo;
    }

    public String getMatricula_aeronave() {
        return matricula_aeronave;
    }

    public void setMatricula_aeronave(String matricula_aeronave) {
        this.matricula_aeronave = matricula_aeronave;
    }

    public String getAerolinea() {
        return aerolinea;
    }

    public void setAerolinea(String aerolinea) {
        this.aerolinea = aerolinea;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public double getHora_vuelo() {
        return hora_vuelo;
    }

    public void setHora_vuelo(double hora_vuelo) {
        this.hora_vuelo = hora_vuelo;
    }

    public double getFecha_vuelo() {
        return fecha_vuelo;
    }

    public void setFecha_vuelo(double fecha_vuelo) {
        this.fecha_vuelo = fecha_vuelo;
    }    

    @Override
    public String toString() {
        return "Vuelo{" + "id_vuelo=" + id_vuelo + ", matricula_aeronave=" + matricula_aeronave + ", aerolinea=" + aerolinea + ", origen=" + origen + ", destino=" + destino + ", hora_vuelo=" + hora_vuelo + ", fecha_vuelo=" + fecha_vuelo + ", tipo_vuelo=" + '}';
    }
    
    // Metodos CRUD
    
    public boolean consultarVueloExiste() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_vuelo, matricula_aeronave, aerolinea, origen, destino, hora_vuelo, fecha_vuelo FROM vuelo WHERE id_vuelo = ?";
        List<Vuelo> vuelos = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Vuelo(
                        rs.getInt("id_vuelo"),
                        rs.getString("matricula_aeronave"),
                        rs.getString("aerolinea"),
                        rs.getString("origen"),
                        rs.getString("destino"),
                        rs.getDouble("hora_vuelo"),
                        rs.getDouble("fecha_vuelo")
                ), new Object[]{this.getId_vuelo()});
        
        if (vuelos != null &&  !vuelos.isEmpty()) {
            this.setMatricula_aeronave(vuelos.get(0).getMatricula_aeronave());
            this.setAerolinea(vuelos.get(0).getAerolinea());
            this.setOrigen(vuelos.get(0).getOrigen());
            this.setDestino(vuelos.get(0).getDestino());
            this.setHora_vuelo(vuelos.get(0).getHora_vuelo());
            this.setFecha_vuelo(vuelos.get(0).getFecha_vuelo());
            return true;
        }
        else {
            return false;
        }
        
    }
    
    public void actualizarVuelo() throws ClassNotFoundException, SQLException{
        String sql = "UPDATE vuelo SET matricula_aeronave = ?, aerolinea = ?, origen = ?, destino = ?, hora_vuelo = ?, fecha_vuelo = ? WHERE id_vuelo = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(2, this.getMatricula_aeronave());
        ps.setString(3, this.getAerolinea());
        ps.setString(4, this.getOrigen());
        ps.setString(4, this.getDestino());
        ps.setDouble(4, this.getHora_vuelo());
        ps.setDouble(4, this.getFecha_vuelo());
        ps.executeUpdate();
        ps.close(); 
    }
    
    public boolean borrarVuelo() throws SQLException, ClassNotFoundException{
    //CRUD -D
    String sql = "DELETE FROM vuelo WHERE id_vuelo = ?";
    Connection c = jdbcTemplate.getDataSource().getConnection();
    PreparedStatement ps = c.prepareStatement(sql);
    ps.setInt(1, this.getId_vuelo());
    ps.execute();
    ps.close();
    return true;
    }

}
