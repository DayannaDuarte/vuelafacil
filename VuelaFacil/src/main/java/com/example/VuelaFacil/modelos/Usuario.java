package com.example.VuelaFacil.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grupo O5-01
 */
@Component("usuario")
public class Usuario {

    // Paquete que ejecuta las consultas SQL.
    @Autowired
    transient JdbcTemplate jdbcTemplate; // Palabra clave en java para serializar.
    // Sirven para demarcar el caracter temporal o trnasitorio de dicha variable

    // Atributos:
    private int id_usuario;
    private String nombre_usuario;
    private String apellido_usuario;
    private String contrasenia_usuario;
    private String correo_usuario;

    // Metodo constructor:
    public Usuario(int id_usuario, String nombre_usuario, String apellido_usuario, String contrasenia_usuario, String correo_usuario) {
        this.id_usuario = id_usuario;
        this.nombre_usuario = nombre_usuario;
        this.apellido_usuario = apellido_usuario;
        this.contrasenia_usuario = contrasenia_usuario;
        this.correo_usuario = correo_usuario;
    }

    public Usuario() {
    }

    // Getters y Setters
    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getApellido_usuario() {
        return apellido_usuario;
    }

    public void setApellido_usuario(String apellido_usuario) {
        this.apellido_usuario = apellido_usuario;
    }

    public String getContrasenia_usuario() {
        return contrasenia_usuario;
    }

    public void setContrasenia_usuario(String contrasenia_usuario) {
        this.contrasenia_usuario = contrasenia_usuario;
    }

    public String getCorreo_usuario() {
        return correo_usuario;
    }

    public void setCorreo_usuario(String correo_usuario) {
        this.correo_usuario = correo_usuario;
    }

    // CRUD - C
    public void guardar() throws ClassNotFoundException, SQLException { // throws para advertir a java de que pueden suceder los errores indicados
        String sql = "INSERT INTO usuario(id_usuario, nombre_usuario, apellido_usuario, contrasenia_usuario, correo_usuario) VALUES(?,?,?,?,?)";
        jdbcTemplate.execute(sql);
    }

    // CRUD - R
    public boolean consultarUsuario() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_usuario, nombre_usuario, apellido_usuario, contrasenia_usuario, correo_usuario FROM usuario WHERE id_usuario";
        List<Usuario> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuario(
                        rs.getInt("id_usuario"),
                        rs.getString("nombre_usuario"),
                        rs.getString("apellido_usuario"),
                        rs.getString("contrasenia_usuario"),
                        rs.getString("correo_usario")
                ), new Object[]{this.getId_usuario()});

        if (usuarios != null && !usuarios.isEmpty()) {
            this.setId_usuario(usuarios.get(0).getId_usuario());
            this.setNombre_usuario(usuarios.get(0).getNombre_usuario());
            this.setApellido_usuario(usuarios.get(0).getApellido_usuario());
            this.setContrasenia_usuario(usuarios.get(0).getContrasenia_usuario());
            this.setCorreo_usuario(usuarios.get(0).getCorreo_usuario());
            return true;
        } else {
            return false;
        }
    }

    public List<Usuario> consultarTodo(int id_usuario) throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_usuario, nombre_usuario, apellido_usuario, contrasenia_usuario, correo_usuario FROM usuario WHERE id_usuario = ?";
        List<Usuario> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuario(
                        rs.getInt("id_usuario"),
                        rs.getString("nombre_usuario"),
                        rs.getString("apellido_usuario"),
                        rs.getString("contrasenia_usuario"),
                        rs.getString("correo_usuario")
                ), new Object[]{id_usuario});
        return usuarios;
    }

    public void actualizarUsuario() throws ClassNotFoundException, SQLException {
        // CRUD -U
        String sql = "UPDATE usuario SET nombre_usuario = ?, apellido_usuario = ?, contrasenia_usuario = ?, correo_usuario = ? WHERE id_usuario = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre_usuario());
        ps.setString(2, this.getApellido_usuario());
        ps.setString(3, this.getContrasenia_usuario());
        ps.setString(4, this.getCorreo_usuario());
        ps.executeUpdate();
        ps.close();
    }
    
    public int guardarUsuario() throws ClassNotFoundException, SQLException {
        int last_inserted_id = -1;
        String sql = "INSERT INTO usuario(nombre_usuario, apellido_usuario, contrasenia_usuario, correo_usuario) VALUES(?,?,?,?)";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre_usuario());
        ps.setString(2, this.getApellido_usuario());
        ps.setString(3, this.getContrasenia_usuario());
        ps.setString(4, this.getCorreo_usuario());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();   // Llave generada del PS -- Insertar Id automaticamente
        if(rs.next()) {
            last_inserted_id = rs.getInt(1);    // Esto permite avanzar a la siguiente posicion/fila
        }
        ps.close();
        return last_inserted_id;
    }

}
