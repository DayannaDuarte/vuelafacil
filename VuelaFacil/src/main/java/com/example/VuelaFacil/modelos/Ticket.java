package com.example.VuelaFacil.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Grupo O5-01
 */
@Component("ticket")
public class Ticket {
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    // Atributos:
    private int id_ticket;
    private Usuario id_usuario; // Llave foranea tabla usuario
    private Vuelo id_vuelo;     // Llave foranea tabla vuelo
    private String clase;
    private int nro_asiento;
    private double costo;
    
    // Constructor:
    public Ticket(int id_ticket, String clase, int nro_asiento, double costo) {
        super();
        this.id_ticket = id_ticket;
        this.clase = clase;
        this.nro_asiento = nro_asiento;
        this.costo = costo;
    }

    public Ticket() {
    }

    public int getId_ticket() {
        return id_ticket;
    }

    public void setId_ticket(int id_ticket) {
        this.id_ticket = id_ticket;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public int getNro_asiento() {
        return nro_asiento;
    }

    public void setNro_asiento(int nro_asiento) {
        this.nro_asiento = nro_asiento;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }
    
    public int getId_usuario() {
        return id_usuario.getId_usuario();
    }
    
    public void setId_usuario(int id_usuario) {
        this.id_usuario.setId_usuario(id_usuario);
    }

    public int getId_vuelo() {
        return id_vuelo.getId_vuelo();
    }
    
    public void setId_vuelo(int id_vuelo) {
        this.id_vuelo.setId_vuelo(id_vuelo);
    }    
    
    @Override
    public String toString() {
        return "Ticket{" + "id_ticket=" + id_ticket + ", clase=" + clase + ", nro_asiento=" + nro_asiento + ", costo=" + costo + '}';
    }
    
    public void generarTicket() throws SQLException, ClassNotFoundException {
        String sql = "INSERT INTO ticket(clase, nro_asiento, costo) VALUES(?, ?, ?)";
        jdbcTemplate.execute(sql);
    }
    
    public boolean borrarTicket() throws SQLException, ClassNotFoundException {
        String sql = "DELETE FROM ticket WHERE id_usuario = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setObject(1, this.getId_usuario());
        ps.execute();
        ps.close();
        return true;
    }
    
    public void actualizarTicket() throws ClassNotFoundException, SQLException{
        // CRUD -U
        String sql = "UPDATE ticket SET clase = ?, nro_asiento = ?, costo = ? WHERE id_ticket = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(2, this.getClase());
        ps.setInt(3, this.getNro_asiento());
        ps.setDouble(4, this.getCosto());
        ps.executeUpdate();
        ps.close();
    }
    
    public List<Ticket> consultarTodo(int id_ticket) throws ClassNotFoundException, SQLException {
    String sql = "SELECT id_ticket, id_usuario, id_vuelo, clase, nro_asiento, costo FROM usuario WHERE id_ticket = ?";
    List<Ticket> tickets = jdbcTemplate.query(sql, (rs, rowNum)
            -> new Ticket(
                    rs.getInt("id_ticket"),
                    rs.getString("clase"),
                    rs.getInt("nro_asiento"),
                    rs.getDouble("costo")
        ), new Object[]{id_ticket});
    return tickets;
    }
}
