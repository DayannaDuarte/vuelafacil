package com.example.VuelaFacil.modelos;

import java.sql.SQLException;

/**
 *
 * @author Grupo O5-01
 */
public class PrimeraClase extends Ticket {
    
    @Override
    public void generarTicket() throws SQLException, ClassNotFoundException {
        this.setClase("Primera Clase");
        super.generarTicket();
    }
}
