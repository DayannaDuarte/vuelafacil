package com.example.VuelaFacil.modelos;

import java.sql.SQLException;

/**
 *
 * @author Andres
 */
public class ClaseEconomica extends Ticket {
    
    @Override
    public void generarTicket() throws SQLException, ClassNotFoundException {
        this.setClase("Clase Económica");
        super.generarTicket();
    }
}
