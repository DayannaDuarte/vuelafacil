// JavaScript source code
angular.module('VuelaFacil',[])
.controller('filasTickets',function($scope,$http){
// Nos ayuda a hacer mas dinamica la pagina e ir agregando datos de forma automatica
    
    $scope.id_usuario = 0;
    $scope.id_vuelo = 0;
    $scope.id_ticket = 0;
    $scope.nombre_usuario = "";
    $scope.apellido_usuario = "";
    $scope.correo_usuario = "";
    $scope.contrasenia_usuario = "";
    
    $scope.id_usuario1 = 0;
    $scope.nombre_usuario1 = "";
    $scope.apellido_usuario1 = "";
    $scope.correo_usuario1 = "";
    $scope.contrasenia_usuario1 = "";
    
    $scope.clase = "";
    $scope.nro_asiento = 0;
    $scope.costo = 0;
    $scope.matricula_aeronave = "";
    $scope.aerolinea = "";
    $scope.origen = "";
    $scope.destino = "";
    $scope.hora_vuelo = 0;
    $scope.fecha_vuelo = 0;
    
    // Accion del boton consultar
    $scope.consultarUsuario = function(){
        if($scope.nombre_usuario === undefined || $scope.nombre_usuario === null){
            $scope.nombre_usuario = "";
        }
        
        // Hacemos una peticion al servidor
        $http.get("/VuelaFacil/usuario?nombre_usuario="+$scope.nombre_usuario).then(function(data){
            console.log(data.data); // Impresion por consola
            $scope.nombre_usuario = data.data.nombre_usuario;
            $scope.apellido_usuario = data.data.apellido_usuario;
            $scope.correo_usuario = data.data.correo_usuario;
            $scope.contrasenia_usuario = data.data.correo_usuario;
        },function(){
             //error
            $scope.nombre_usuario = "";
            $scope.apellido_usuario = "";
            $scope.correo_usuario = "";
            $scope.contrasenia_usuario = "";
            $scope.filas = []; // guarda
        });
        
        $http.get("/VuelaFacil/tickets?id_usuario="+$scope.id_usuario).then(function(data){
            console.log(data.data); // Mensaje en consola
            $scope.filas = data.data;  
        },function(){
            $scope.filas = []; // Llenar la lista con los tickets por Id de usuario
        });
    };
    
    //Acción del botón actualizar
    $scope.actualizarUsuario = function(){
        if($scope.nombre_usuario === undefined || $scope.nombre_usuario === null){
            $scope.nombre_usuario = "";
        }
        data = {
            "id_usuario": $scope.id_usuario,
            "nombre_usuario":$scope.nombre_usuario,
            "apellido_usuario": $scope.apellido_usuario,
            "correo_usuario": $scope.correo_usuario,
            "contrasenia_usuario": $scope.contrasenia_usuario
        };
        $http.post('/VuelaFacil/usuario', data).then(function(data){
            //success
            $scope.nombre_usuario = data.data.nombre_usuario;
            $scope.apellido_usuario = data.data.apellido_usuario;
            $scope.correo_usuario = data.data.correo_usuario;
            $scope.contrasenia_usuario = data.data.correo_usuario;
        },function(){
            //error
            $scope.nombre_usuario = "";
            $scope.apellido_usuario = "";
            $scope.correo_usuario = "";
            $scope.contrasenia_usuario = "";
            $scope.filas = []; // guarda
        });
        
    };
    
    //Acción del botón guardar
    $scope.guardarUsuario = function(){
        data = {
            "id_usuario": $scope.id_usuario1, // Aqui en el del ConsultorioOnline Ponian las variables con 1 en el nombre
            "nombre_usuario":$scope.nombre_usuario1,
            "apellido_usuario": $scope.apellido_usuario1,
            "correo_usuario": $scope.correo_usuario1,
            "contrasenia_usuario": $scope.contrasenia_usuario1
        };
        $http.post('/VuelaFacil/guardarusuario', data).then(function(data){
            //success
            $scope.nombre_usuario1 = data.data.nombre_usuario1; // Aqui en el del ConsultorioOnline Ponian las variables con 1 en el nombre
            $scope.apellido_usuario1 = data.data.apellido_usuario1;
            $scope.correo_usuario1 = data.data.correo_usuario1;
            $scope.contrasenia_usuario1 = data.data.correo_usuario1;
        },function(){
            //error
            $scope.nombre_usuario1 = ""; // Aqui en el del ConsultorioOnline Ponian las variables con 1 en el nombre
            $scope.apellido_usuario1 = "";
            $scope.correo_usuario1 = "";
            $scope.contrasenia_usuario1 = "";
            $scope.filas1 = []; // guarda
        });
        
    };
    
    
    // Acción del botón borrar
    $scope.borrarTicket = function(){
        if($scope.id_usuario === undefined || $scope.id_usuario === null){
            $scope.id_usuario = 0;
        }
        data = {
            "id_usuario": $scope.id_usuario
        };
        $http.delete('/VuelaFacil/eliminarticket/'+$scope.id_usuario, data).then(function(data){
            alert("La cuenta ha sido eliminada");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };
});


